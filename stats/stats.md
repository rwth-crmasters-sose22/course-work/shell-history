| `command` | number of doers | names of doers |
| --------- | --------------- | -------------- |
| `pwd` | 23 | Wei Yang, Yu Wang, Enes Ucmaz, Htoo Inzali, Ye Lu.txt, Ramez Tawfik, Omar Yousef, Rodrigo Magan, Rebeca Garjul, Camilo Bedoya, Noel Joseph, Shady Saleh, Miguel Gutierrez, Youssef Osama, Sushmitha Pais, Ke Ren, Orwa Diraneyya, Jaime Romero, Beyza Irmak, David Lukert, Robin Berweiler, Bora Karadeniz, Mia Obralic |
| `ls` | 23 | Wei Yang, Yu Wang, Enes Ucmaz, Htoo Inzali, Ye Lu.txt, Ramez Tawfik, Omar Yousef, Rodrigo Magan, Rebeca Garjul, Camilo Bedoya, Noel Joseph, Shady Saleh, Miguel Gutierrez, Youssef Osama, Sushmitha Pais, Ke Ren, Orwa Diraneyya, Jaime Romero, Beyza Irmak, David Lukert, Robin Berweiler, Bora Karadeniz, Mia Obralic |
| `exit` | 22 | Wei Yang, Yu Wang, Enes Ucmaz, Htoo Inzali, Ramez Tawfik, Omar Yousef, Rodrigo Magan, Rebeca Garjul, Camilo Bedoya, Noel Joseph, Shady Saleh, Miguel Gutierrez, Youssef Osama, Sushmitha Pais, Ke Ren, Orwa Diraneyya, Jaime Romero, Beyza Irmak, David Lukert, Robin Berweiler, Bora Karadeniz, Mia Obralic |
| `cd` | 21 | Wei Yang, Yu Wang, Enes Ucmaz, Htoo Inzali, Ye Lu.txt, Ramez Tawfik, Omar Yousef, Rodrigo Magan, Rebeca Garjul, Camilo Bedoya, Noel Joseph, Shady Saleh, Miguel Gutierrez, Youssef Osama, Sushmitha Pais, Orwa Diraneyya, Jaime Romero, Beyza Irmak, David Lukert, Robin Berweiler, Bora Karadeniz |
| `cd ..` | 21 | Wei Yang, Yu Wang, Enes Ucmaz, Htoo Inzali, Ramez Tawfik, Omar Yousef, Rodrigo Magan, Rebeca Garjul, Camilo Bedoya, Noel Joseph, Shady Saleh, Miguel Gutierrez, Youssef Osama, Sushmitha Pais, Ke Ren, Orwa Diraneyya, Jaime Romero, Beyza Irmak, David Lukert, Robin Berweiler, Bora Karadeniz |
| `whoami` | 20 | Wei Yang, Yu Wang, Enes Ucmaz, Htoo Inzali, Ramez Tawfik, Omar Yousef, Rodrigo Magan, Rebeca Garjul, Camilo Bedoya, Noel Joseph, Shady Saleh, Miguel Gutierrez, Sushmitha Pais, Orwa Diraneyya, Jaime Romero, Beyza Irmak, David Lukert, Robin Berweiler, Bora Karadeniz, Mia Obralic |
| `man cd` | 19 | Wei Yang, Yu Wang, Enes Ucmaz, Htoo Inzali, Ramez Tawfik, Omar Yousef, Rebeca Garjul, Camilo Bedoya, Noel Joseph, Shady Saleh, Miguel Gutierrez, Youssef Osama, Sushmitha Pais, Orwa Diraneyya, Jaime Romero, Beyza Irmak, David Lukert, Robin Berweiler, Bora Karadeniz |
| `man ls` | 19 | Wei Yang, Enes Ucmaz, Htoo Inzali, Ramez Tawfik, Omar Yousef, Rodrigo Magan, Rebeca Garjul, Camilo Bedoya, Noel Joseph, Shady Saleh, Miguel Gutierrez, Sushmitha Pais, Orwa Diraneyya, Jaime Romero, Beyza Irmak, David Lukert, Robin Berweiler, Bora Karadeniz, Mia Obralic |
| `ls -a` | 19 | Yu Wang, Enes Ucmaz, Htoo Inzali, Ramez Tawfik, Omar Yousef, Rodrigo Magan, Rebeca Garjul, Camilo Bedoya, Noel Joseph, Shady Saleh, Miguel Gutierrez, Sushmitha Pais, Orwa Diraneyya, Jaime Romero, Beyza Irmak, David Lukert, Robin Berweiler, Bora Karadeniz, Mia Obralic |
| `man pwd` | 18 | Rodrigo Magan, Wei Yang, Rebeca Garjul, Sushmitha Pais, Omar Yousef, David Lukert, Orwa Diraneyya, Jaime Romero, Ramez Tawfik, Robin Berweiler, Enes Ucmaz, Htoo Inzali, Miguel Gutierrez, Noel Joseph, Bora Karadeniz, Shady Saleh, Beyza Irmak, Youssef Osama |
| `touch hello.rb` | 18 | Rodrigo Magan, Wei Yang, Rebeca Garjul, Sushmitha Pais, Omar Yousef, David Lukert, Ke Ren, Yu Wang, Orwa Diraneyya, Jaime Romero, Robin Berweiler, Enes Ucmaz, Miguel Gutierrez, Noel Joseph, Bora Karadeniz, Beyza Irmak, Youssef Osama, Mia Obralic |
| `man touch` | 18 | Wei Yang, Rebeca Garjul, Sushmitha Pais, Omar Yousef, David Lukert, Camilo Bedoya, Yu Wang, Orwa Diraneyya, Jaime Romero, Ramez Tawfik, Robin Berweiler, Enes Ucmaz, Miguel Gutierrez, Noel Joseph, Bora Karadeniz, Shady Saleh, Beyza Irmak, Youssef Osama |
| `ruby --version` | 17 | Rodrigo Magan, Wei Yang, Rebeca Garjul, Sushmitha Pais, Omar Yousef, Camilo Bedoya, Ke Ren, Jaime Romero, Enes Ucmaz, Htoo Inzali, Miguel Gutierrez, Noel Joseph, Bora Karadeniz, Shady Saleh, Beyza Irmak, Youssef Osama, Mia Obralic |
| `python --version` | 17 | Rodrigo Magan, Wei Yang, Rebeca Garjul, Sushmitha Pais, Omar Yousef, Camilo Bedoya, Ke Ren, Jaime Romero, Enes Ucmaz, Htoo Inzali, Miguel Gutierrez, Noel Joseph, Bora Karadeniz, Shady Saleh, Beyza Irmak, Youssef Osama, Mia Obralic |
| `ruby hello.rb` | 17 | Rodrigo Magan, Rebeca Garjul, Sushmitha Pais, Omar Yousef, David Lukert, Ke Ren, Yu Wang, Orwa Diraneyya, Jaime Romero, Robin Berweiler, Enes Ucmaz, Miguel Gutierrez, Bora Karadeniz, Shady Saleh, Beyza Irmak, Youssef Osama, Mia Obralic |
| `man cat` | 17 | Wei Yang, Rebeca Garjul, Sushmitha Pais, Omar Yousef, David Lukert, Camilo Bedoya, Ke Ren, Yu Wang, Orwa Diraneyya, Jaime Romero, Robin Berweiler, Enes Ucmaz, Miguel Gutierrez, Ramez Tawfik, Bora Karadeniz, Beyza Irmak, Youssef Osama |
| `man mkdir` | 16 | Wei Yang, Rebeca Garjul, Sushmitha Pais, Omar Yousef, David Lukert, Orwa Diraneyya, Jaime Romero, Robin Berweiler, Enes Ucmaz, Htoo Inzali, Miguel Gutierrez, Ramez Tawfik, Bora Karadeniz, Shady Saleh, Beyza Irmak, Youssef Osama |
| `node --version` | 15 | Rodrigo Magan, Rebeca Garjul, Sushmitha Pais, Omar Yousef, Camilo Bedoya, Jaime Romero, Enes Ucmaz, Htoo Inzali, Miguel Gutierrez, Noel Joseph, Bora Karadeniz, Shady Saleh, Beyza Irmak, Youssef Osama, Mia Obralic |
| `ls -F` | 15 | Rebeca Garjul, Sushmitha Pais, Omar Yousef, David Lukert, Yu Wang, Orwa Diraneyya, Jaime Romero, Robin Berweiler, Enes Ucmaz, Htoo Inzali, Miguel Gutierrez, Bora Karadeniz, Shady Saleh, Beyza Irmak, Youssef Osama |
| `cd ~` | 15 | Rodrigo Magan, Wei Yang, Rebeca Garjul, Sushmitha Pais, Omar Yousef, Yu Wang, Camilo Bedoya, Orwa Diraneyya, Jaime Romero, Enes Ucmaz, Htoo Inzali, Noel Joseph, Bora Karadeniz, Shady Saleh, Beyza Irmak |
| `node hello.js` | 15 | Rodrigo Magan, Rebeca Garjul, Sushmitha Pais, Omar Yousef, David Lukert, Camilo Bedoya, Jaime Romero, Robin Berweiler, Enes Ucmaz, Miguel Gutierrez, Bora Karadeniz, Shady Saleh, Beyza Irmak, Youssef Osama, Mia Obralic |
| `python hello.py` | 15 | Rodrigo Magan, Rebeca Garjul, Sushmitha Pais, Omar Yousef, David Lukert, Camilo Bedoya, Jaime Romero, Robin Berweiler, Enes Ucmaz, Miguel Gutierrez, Bora Karadeniz, Shady Saleh, Beyza Irmak, Youssef Osama, Mia Obralic |
| `touch hello.js` | 14 | Rodrigo Magan, Wei Yang, Rebeca Garjul, Sushmitha Pais, Omar Yousef, David Lukert, Camilo Bedoya, Jaime Romero, Robin Berweiler, Miguel Gutierrez, Bora Karadeniz, Shady Saleh, Beyza Irmak, Youssef Osama |
| `touch hello.py` | 14 | Rodrigo Magan, Rebeca Garjul, Sushmitha Pais, Omar Yousef, David Lukert, Camilo Bedoya, Jaime Romero, Robin Berweiler, Enes Ucmaz, Miguel Gutierrez, Bora Karadeniz, Shady Saleh, Beyza Irmak, Youssef Osama |
| `cat hello.rb` | 13 | Rodrigo Magan, Rebeca Garjul, Sushmitha Pais, Omar Yousef, David Lukert, Yu Wang, Jaime Romero, Enes Ucmaz, Miguel Gutierrez, Bora Karadeniz, Shady Saleh, Beyza Irmak, Youssef Osama |
| `cd .` | 13 | Wei Yang, Rebeca Garjul, Sushmitha Pais, Omar Yousef, Yu Wang, Camilo Bedoya, Orwa Diraneyya, Robin Berweiler, Enes Ucmaz, Htoo Inzali, Jaime Romero, Shady Saleh, Miguel Gutierrez |
| `npm --version` | 12 | Rodrigo Magan, Sushmitha Pais, Omar Yousef, Camilo Bedoya, Jaime Romero, Enes Ucmaz, Miguel Gutierrez, Noel Joseph, Bora Karadeniz, Shady Saleh, Beyza Irmak, Youssef Osama |
| `ls -1` | 12 | Sushmitha Pais, Omar Yousef, David Lukert, Jaime Romero, Robin Berweiler, Enes Ucmaz, Htoo Inzali, Noel Joseph, Ramez Tawfik, Bora Karadeniz, Shady Saleh, Miguel Gutierrez |
| `ls -l -a` | 11 | Rodrigo Magan, Sushmitha Pais, Omar Yousef, David Lukert, Ke Ren, Jaime Romero, Robin Berweiler, Ramez Tawfik, Bora Karadeniz, Shady Saleh, Miguel Gutierrez |
| `irb` | 11 | Wei Yang, Rebeca Garjul, Sushmitha Pais, David Lukert, Yu Wang, Orwa Diraneyya, Enes Ucmaz, Noel Joseph, Jaime Romero, Shady Saleh, Miguel Gutierrez |
| `ls -d` | 11 | Rodrigo Magan, Wei Yang, Sushmitha Pais, Jaime Romero, Robin Berweiler, Htoo Inzali, Noel Joseph, Ramez Tawfik, Omar Yousef, Shady Saleh, Miguel Gutierrez |
| `irb --version` | 10 | Rodrigo Magan, Sushmitha Pais, Omar Yousef, Camilo Bedoya, Jaime Romero, Enes Ucmaz, Noel Joseph, Bora Karadeniz, Shady Saleh, Beyza Irmak |
| `erb --version` | 10 | Rodrigo Magan, Sushmitha Pais, Omar Yousef, Camilo Bedoya, Jaime Romero, Enes Ucmaz, Noel Joseph, Bora Karadeniz, Shady Saleh, Beyza Irmak |
| `pip --version` | 10 | Rodrigo Magan, Sushmitha Pais, Omar Yousef, Camilo Bedoya, Jaime Romero, Enes Ucmaz, Noel Joseph, Bora Karadeniz, Shady Saleh, Beyza Irmak |
| `man mv` | 10 | Wei Yang, Rebeca Garjul, Sushmitha Pais, David Lukert, Jaime Romero, Robin Berweiler, Ramez Tawfik, Bora Karadeniz, Shady Saleh, Miguel Gutierrez |
| `man cp` | 10 | Wei Yang, Rebeca Garjul, Sushmitha Pais, David Lukert, Jaime Romero, Robin Berweiler, Ramez Tawfik, Bora Karadeniz, Shady Saleh, Miguel Gutierrez |
| `ls -l -h` | 9 | Rodrigo Magan, Wei Yang, Sushmitha Pais, Omar Yousef, David Lukert, Jaime Romero, Enes Ucmaz, Bora Karadeniz, Shady Saleh |
| `man rm` | 9 | Rebeca Garjul, Sushmitha Pais, David Lukert, Jaime Romero, Robin Berweiler, Htoo Inzali, Ramez Tawfik, Bora Karadeniz, Shady Saleh |
| `ls -l -t -r` | 9 | Sushmitha Pais, Omar Yousef, David Lukert, Robin Berweiler, Enes Ucmaz, Htoo Inzali, Jaime Romero, Shady Saleh, Miguel Gutierrez |
| `cat hello.py` | 8 | Rodrigo Magan, Bora Karadeniz, Sushmitha Pais, Jaime Romero, Beyza Irmak, Omar Yousef, Miguel Gutierrez, Youssef Osama |
| `cd root` | 8 | Rebeca Garjul, Omar Yousef, Camilo Bedoya, Ke Ren, Robin Berweiler, Ramez Tawfik, Bora Karadeniz, Shady Saleh |
| `cat hello.js` | 8 | Rodrigo Magan, Bora Karadeniz, Sushmitha Pais, Jaime Romero, Robin Berweiler, Omar Yousef, Miguel Gutierrez, Youssef Osama |
| `yarn --version` | 8 | Rebeca Garjul, Sushmitha Pais, Yu Wang, Camilo Bedoya, Orwa Diraneyya, Noel Joseph, Omar Yousef, Miguel Gutierrez |
| `cd /` | 8 | Rebeca Garjul, Sushmitha Pais, Orwa Diraneyya, Jaime Romero, Enes Ucmaz, Noel Joseph, Omar Yousef, Mia Obralic |
| `cd ` | 7 | Wei Yang, Rebeca Garjul, Orwa Diraneyya, Jaime Romero, Bora Karadeniz, Miguel Gutierrez, Youssef Osama |
| `cd tmp` | 7 | Rodrigo Magan, Rebeca Garjul, Sushmitha Pais, Yu Wang, Orwa Diraneyya, Omar Yousef, Youssef Osama |
| `ls ` | 6 | Rebeca Garjul, Htoo Inzali, Noel Joseph, Miguel Gutierrez, Bora Karadeniz, Beyza Irmak |
| `code hello.rb` | 6 | Sushmitha Pais, David Lukert, Yu Wang, Jaime Romero, Beyza Irmak, Youssef Osama |
| `rm -r` | 6 | Rebeca Garjul, Sushmitha Pais, Robin Berweiler, Enes Ucmaz, Htoo Inzali, Bora Karadeniz |
| `cd class03` | 6 | Rodrigo Magan, Jaime Romero, Bora Karadeniz, Shady Saleh, Youssef Osama, Mia Obralic |
| `man` | 6 | David Lukert, Jaime Romero, Robin Berweiler, Noel Joseph, Omar Yousef, Youssef Osama |
| `man whoami` | 6 | Wei Yang, Sushmitha Pais, David Lukert, Htoo Inzali, Ramez Tawfik, Jaime Romero |
| `ls -l` | 6 | Wei Yang, Rebeca Garjul, Sushmitha Pais, Ke Ren, Htoo Inzali, Ramez Tawfik |
| `cat` | 6 | Robin Berweiler, Ye Lu.txt, Noel Joseph, Ramez Tawfik, Youssef Osama, Mia Obralic |
| `node` | 5 | Rebeca Garjul, Camilo Bedoya, Robin Berweiler, Beyza Irmak, Youssef Osama |
| `mkdir` | 5 | Omar Yousef, Enes Ucmaz, Ye Lu.txt, Bora Karadeniz, Youssef Osama |
| `mkdir class03` | 5 | Rodrigo Magan, Jaime Romero, Bora Karadeniz, Youssef Osama, Mia Obralic |
| `clear` | 5 | Wei Yang, Camilo Bedoya, Orwa Diraneyya, Htoo Inzali, Jaime Romero |
| `cd..` | 4 | Beyza Irmak, Youssef Osama, Noel Joseph, David Lukert |
| `cat ruby.rb` | 4 | Omar Yousef, Rebeca Garjul, Beyza Irmak, Miguel Gutierrez |
| `touch` | 4 | Bora Karadeniz, Ye Lu.txt, Rebeca Garjul, Youssef Osama |
| `~` | 4 | Bora Karadeniz, Ye Lu.txt, Rodrigo Magan, Shady Saleh |
| `history` | 4 | Bora Karadeniz, Htoo Inzali, Jaime Romero, Robin Berweiler |
| `ls -l -S` | 4 | Robin Berweiler, Enes Ucmaz, Sushmitha Pais, David Lukert |
| `pwd --logical` | 4 | Robin Berweiler, Ramez Tawfik, Wei Yang, David Lukert |
| `pwd --physical` | 4 | Robin Berweiler, Htoo Inzali, Wei Yang, Ramez Tawfik |
| `python` | 4 | Shady Saleh, Htoo Inzali, Rebeca Garjul, Wei Yang |
| `l` | 4 | Rodrigo Magan, Orwa Diraneyya, Sushmitha Pais, Noel Joseph |
| `apt update` | 3 | Orwa Diraneyya, Beyza Irmak, Yu Wang |
| `apt install -y` | 3 | Orwa Diraneyya, Beyza Irmak, Yu Wang |
| `ls ~` | 3 | Camilo Bedoya, Htoo Inzali, Sushmitha Pais |
| `cp -r` | 3 | Wei Yang, Sushmitha Pais, David Lukert |
| `cal` | 3 | Htoo Inzali, Shady Saleh, Miguel Gutierrez |
| `ls -l -s` | 3 | Jaime Romero, Shady Saleh, Robin Berweiler |
| `/` | 3 | Ye Lu.txt, Rebeca Garjul, Mia Obralic |
| `man npm` | 2 | Robin Berweiler, Beyza Irmak |
| `cat ruby.py` | 2 | Shady Saleh, Beyza Irmak |
| `cd hello.rb` | 2 | Bora Karadeniz, Beyza Irmak |
| `ruby` | 2 | Bora Karadeniz, Htoo Inzali |
| `cd home` | 2 | Bora Karadeniz, Rodrigo Magan |
| `cd hello` | 2 | Ke Ren, Bora Karadeniz |
| `cd media` | 2 | Bora Karadeniz, Noel Joseph |
| `rm hello.rb` | 2 | Bora Karadeniz, Jaime Romero |
| `ls a` | 2 | Bora Karadeniz, Rebeca Garjul |
| `cd test1` | 2 | Camilo Bedoya, Rebeca Garjul |
| `mkdir test` | 2 | Camilo Bedoya, Wei Yang |
| `cd test` | 2 | Camilo Bedoya, Wei Yang |
| `.` | 2 | Camilo Bedoya, Htoo Inzali |
| `..` | 2 | Camilo Bedoya, Htoo Inzali |
| `cd test/` | 2 | Camilo Bedoya, Wei Yang |
| `javascript hello.js` | 2 | Youssef Osama, David Lukert |
| `touch -d` | 2 | Wei Yang, David Lukert |
| `cat -n` | 2 | Ramez Tawfik, David Lukert |
| `cd root/` | 2 | Htoo Inzali, David Lukert |
| `touch hello` | 2 | Enes Ucmaz, Yu Wang |
| `mkdir root` | 2 | Enes Ucmaz, Shady Saleh |
| `mkdir -p` | 2 | Wei Yang, Enes Ucmaz |
| `rm -r -f` | 2 | Enes Ucmaz, Rebeca Garjul |
| `mkdir hello_ruby` | 2 | Wei Yang, Enes Ucmaz |
| `mkdir hello_python` | 2 | Wei Yang, Enes Ucmaz |
| `mkdir hello_nodejs` | 2 | Wei Yang, Enes Ucmaz |
| `python version` | 2 | Htoo Inzali, Noel Joseph |
| `la -d` | 2 | Htoo Inzali, Noel Joseph |
| `date` | 2 | Htoo Inzali, Miguel Gutierrez |
| `ls /root` | 2 | Htoo Inzali, Sushmitha Pais |
| `cd /root` | 2 | Htoo Inzali, Shady Saleh |
| `ls ..` | 2 | Htoo Inzali, Sushmitha Pais |
| `mkdir --help` | 2 | Robin Berweiler, Htoo Inzali |
| `ruby version` | 2 | Htoo Inzali, Noel Joseph |
| `node ` | 2 | Jaime Romero, Htoo Inzali |
| `mv hello.py root` | 2 | Jaime Romero, Shady Saleh |
| `hello.rb` | 2 | Jaime Romero, Sushmitha Pais |
| `touch hello.txt` | 2 | Jaime Romero, Youssef Osama |
| `mv hello.py class03` | 2 | Jaime Romero, Youssef Osama |
| `mv hello.js class03` | 2 | Jaime Romero, Youssef Osama |
| `man ps` | 2 | Jaime Romero, Youssef Osama |
| `man kill` | 2 | Jaime Romero, Robin Berweiler |
| `kill -l` | 2 | Jaime Romero, Robin Berweiler |
| `man grep` | 2 | Jaime Romero, Robin Berweiler |
| `man time` | 2 | Jaime Romero, Robin Berweiler |
| `time ls` | 2 | Jaime Romero, Robin Berweiler |
| `man less` | 2 | Jaime Romero, Miguel Gutierrez |
| `man chmod` | 2 | Jaime Romero, Robin Berweiler |
| `node -v` | 2 | Ke Ren, Wei Yang |
| `npm -v` | 2 | Ke Ren, Wei Yang |
| `yarn -v` | 2 | Ke Ren, Wei Yang |
| `python.py` | 2 | Ramez Tawfik, Mia Obralic |
| `ls -f` | 2 | Miguel Gutierrez, Youssef Osama |
| `get-alias ls` | 2 | Miguel Gutierrez, Noel Joseph |
| `cd .bash_history` | 2 | Noel Joseph, Ramez Tawfik |
| ` cd ..` | 2 | Ye Lu.txt, Noel Joseph |
| `cd lib64` | 2 | Orwa Diraneyya, Sushmitha Pais |
| `cd ..l` | 2 | Orwa Diraneyya, Sushmitha Pais |
| `cat hello.rb ` | 2 | Wei Yang, Orwa Diraneyya |
| `ruby hello.rb ` | 2 | Wei Yang, Orwa Diraneyya |
| `node hello.js ` | 2 | Wei Yang, Orwa Diraneyya |
| `python hello.py ` | 2 | Wei Yang, Orwa Diraneyya |
| `sudo whoami` | 2 | Wei Yang, Ramez Tawfik |
| `touch ` | 2 | Youssef Osama, Ramez Tawfik |
| `touch -t` | 2 | Robin Berweiler, Ramez Tawfik |
| `ls-1` | 2 | Shady Saleh, Ramez Tawfik |
| `man rv` | 2 | Rebeca Garjul, Ramez Tawfik |
| `cat -v -t -e` | 2 | Wei Yang, Ramez Tawfik |
| `la -a` | 2 | Rebeca Garjul, Sushmitha Pais |
| `touch file1` | 2 | Rebeca Garjul, Sushmitha Pais |
| `cat ruby.js` | 2 | Rebeca Garjul, Youssef Osama |
| `mkdir -v` | 2 | Robin Berweiler, Sushmitha Pais |
| ` ls -l -h` | 2 | Robin Berweiler, Ye Lu.txt |
| ` ls -d` | 2 | Ye Lu.txt, Sushmitha Pais |
| ` cat -v -t -e` | 2 | Ye Lu.txt, Sushmitha Pais |
| `mkdir ` | 2 | Wei Yang, Youssef Osama |
| `root` | 2 | Ye Lu.txt, Youssef Osama |
| `nano hello.rb` | 1 | Beyza Irmak |
| `cd beyza` | 1 | Beyza Irmak |
| `mkdir beyza` | 1 | Beyza Irmak |
| `pyhton hello.py` | 1 | Beyza Irmak |
| `cd run` | 1 | Bora Karadeniz |
| `cd mnt` | 1 | Bora Karadeniz |
| `manmkdir` | 1 | Bora Karadeniz |
| `cd bin` | 1 | Bora Karadeniz |
| `cd _` | 1 | Bora Karadeniz |
| `cd data` | 1 | Bora Karadeniz |
| `cd usr` | 1 | Bora Karadeniz |
| `cd games` | 1 | Bora Karadeniz |
| `mkdir bora` | 1 | Bora Karadeniz |
| `cd bora` | 1 | Bora Karadeniz |
| `rm bora` | 1 | Bora Karadeniz |
| `mkdir jupp` | 1 | Bora Karadeniz |
| `touch move.rb` | 1 | Bora Karadeniz |
| `mv move.rb jupp` | 1 | Bora Karadeniz |
| `cd~` | 1 | Bora Karadeniz |
| `cd jupp` | 1 | Bora Karadeniz |
| `rm jupp` | 1 | Bora Karadeniz |
| `mkdir jupp1` | 1 | Bora Karadeniz |
| `mkdir jupp2` | 1 | Bora Karadeniz |
| `cd jupp1` | 1 | Bora Karadeniz |
| `mv move.rb jupp2/` | 1 | Bora Karadeniz |
| `mv move.rb jupp2` | 1 | Bora Karadeniz |
| `rm jupp2` | 1 | Bora Karadeniz |
| `cd jupp2` | 1 | Bora Karadeniz |
| `mv jupp2 move.rb` | 1 | Bora Karadeniz |
| `volume create` | 1 | Bora Karadeniz |
| `pip` | 1 | Camilo Bedoya |
| `pyton` | 1 | Camilo Bedoya |
| `cd install.sh ` | 1 | Camilo Bedoya |
| `mkdir test1` | 1 | Camilo Bedoya |
| `manned` | 1 | Camilo Bedoya |
| `git info` | 1 | Camilo Bedoya |
| `git --help` | 1 | Camilo Bedoya |
| `mkdir git_test` | 1 | Camilo Bedoya |
| `cd git_test/` | 1 | Camilo Bedoya |
| `git init` | 1 | Camilo Bedoya |
| `cat test1.txt` | 1 | Camilo Bedoya |
| `cat >test1.txt` | 1 | Camilo Bedoya |
| `git add test1.txt` | 1 | Camilo Bedoya |
| `git log` | 1 | Camilo Bedoya |
| `git commit` | 1 | Camilo Bedoya |
| `git config --global` | 1 | Camilo Bedoya |
| `git status` | 1 | Camilo Bedoya |
| `git add .` | 1 | Camilo Bedoya |
| `git commit -m` | 1 | Camilo Bedoya |
| `git shot test1.txt` | 1 | Camilo Bedoya |
| `git show test1.txt` | 1 | Camilo Bedoya |
| `touch Hello.rb` | 1 | Camilo Bedoya |
| `code Hello.rb` | 1 | Camilo Bedoya |
| `ruby Hellor.rb` | 1 | Camilo Bedoya |
| `ruby Hello.rb` | 1 | Camilo Bedoya |
| `git add Hello.rb` | 1 | Camilo Bedoya |
| `git .` | 1 | Camilo Bedoya |
| `pw` | 1 | Camilo Bedoya |
| `man cd ~` | 1 | Camilo Bedoya |
| `find $root -n -a -m -e -l -s` | 1 | Camilo Bedoya |
| `mv test test1` | 1 | Camilo Bedoya |
| `mkdir folder` | 1 | Camilo Bedoya |
| `mv test1 folder` | 1 | Camilo Bedoya |
| `cd folder` | 1 | Camilo Bedoya |
| `mv -i` | 1 | Camilo Bedoya |
| `touch bingo.txt` | 1 | Camilo Bedoya |
| `touch bingo1.txt` | 1 | Camilo Bedoya |
| `rm bingo1.txt ` | 1 | Camilo Bedoya |
| `cat >lingo.txt` | 1 | Camilo Bedoya |
| `cat lingo.txt` | 1 | Camilo Bedoya |
| `cat >lingo2.txt` | 1 | Camilo Bedoya |
| `cat lingo.txt lingo2.txt` | 1 | Camilo Bedoya |
| `mkdir lingo_tests` | 1 | Camilo Bedoya |
| `mv lingo.txt lingo2.txt lingo_tests` | 1 | Camilo Bedoya |
| `whoamI` | 1 | David Lukert |
| `pdw` | 1 | David Lukert |
| `cd .. ` | 1 | David Lukert |
| `show` | 1 | David Lukert |
| `lss -l -h` | 1 | David Lukert |
| `touch -c` | 1 | David Lukert |
| `mkdir david` | 1 | David Lukert |
| `cd david/` | 1 | David Lukert |
| `code hello.py` | 1 | David Lukert |
| `js hello.js` | 1 | David Lukert |
| `man c` | 1 | David Lukert |
| `mkdir copied_files` | 1 | David Lukert |
| `cp hello.js hello_copy.js` | 1 | David Lukert |
| `mv hello_copy.js copied_files/` | 1 | David Lukert |
| `cd copied_files` | 1 | David Lukert |
| `cp hello.rb copied_files/hello_copied.rb` | 1 | David Lukert |
| `mkdir copied_second` | 1 | David Lukert |
| `cd copied_second/` | 1 | David Lukert |
| `cd /root/` | 1 | David Lukert |
| `cd /david` | 1 | David Lukert |
| `pyton hello.py` | 1 | David Lukert |
| `Get-Help cd` | 1 | David Lukert |
| `New_Item test.py` | 1 | David Lukert |
| `version ruby` | 1 | Enes Ucmaz |
| `version --ruby` | 1 | Enes Ucmaz |
| `pyton --version` | 1 | Enes Ucmaz |
| `mkdir enes` | 1 | Enes Ucmaz |
| `cd enes` | 1 | Enes Ucmaz |
| `mkdan mkdir` | 1 | Enes Ucmaz |
| `mkdir hello.rb` | 1 | Enes Ucmaz |
| `cat hello` | 1 | Enes Ucmaz |
| `man hello.rb` | 1 | Enes Ucmaz |
| `cd ../hello.rb/` | 1 | Enes Ucmaz |
| `cd ../hello/` | 1 | Enes Ucmaz |
| `cd ../root/` | 1 | Enes Ucmaz |
| `rmdir hello.rb` | 1 | Enes Ucmaz |
| `rmdir hello` | 1 | Enes Ucmaz |
| `mkdir Hello Ruby` | 1 | Enes Ucmaz |
| `mkdir Hello Python` | 1 | Enes Ucmaz |
| `cd hello_ruby` | 1 | Enes Ucmaz |
| `mkdir hello_node` | 1 | Enes Ucmaz |
| `cd hello_python` | 1 | Enes Ucmaz |
| `cd hello_node` | 1 | Enes Ucmaz |
| `touch hello_ruby/hello.rb` | 1 | Enes Ucmaz |
| `touch hello_python/hello.py` | 1 | Enes Ucmaz |
| `touch hello_nodejs/hello.js` | 1 | Enes Ucmaz |
| `cd hello_nodejs` | 1 | Enes Ucmaz |
| `node hello.rb` | 1 | Enes Ucmaz |
| ` cd hello_nodejs` | 1 | Enes Ucmaz |
| `docker start -a -i` | 1 | Htoo Inzali |
| `ls -d -* -/` | 1 | Htoo Inzali |
| `echo hey!` | 1 | Htoo Inzali |
| `touch anothernewfile` | 1 | Htoo Inzali |
| `file mountain.txt` | 1 | Htoo Inzali |
| `file image.jpg` | 1 | Htoo Inzali |
| `man file` | 1 | Htoo Inzali |
| `file testfile` | 1 | Htoo Inzali |
| `file anothernewfile` | 1 | Htoo Inzali |
| `cat anothernewfile` | 1 | Htoo Inzali |
| `cd anothernewfile` | 1 | Htoo Inzali |
| `copy anothernewfile` | 1 | Htoo Inzali |
| `touch theothernewfile` | 1 | Htoo Inzali |
| `move anothernewfile theothernewfile` | 1 | Htoo Inzali |
| `mv anothernewfile theothernewfile` | 1 | Htoo Inzali |
| `file theothernewfile` | 1 | Htoo Inzali |
| `cat theothernewfile` | 1 | Htoo Inzali |
| `mkdir noroot` | 1 | Htoo Inzali |
| `ls .` | 1 | Htoo Inzali |
| `mkdir fakeroot` | 1 | Htoo Inzali |
| `mv fakeroot noroot` | 1 | Htoo Inzali |
| `mkdir D_1 D_2` | 1 | Htoo Inzali |
| `file noroot` | 1 | Htoo Inzali |
| `rm D_1 D_2` | 1 | Htoo Inzali |
| `rmdir D_1` | 1 | Htoo Inzali |
| `rm -i` | 1 | Htoo Inzali |
| `rm anothernewfile` | 1 | Htoo Inzali |
| `rm the othernewfile` | 1 | Htoo Inzali |
| `rm -f` | 1 | Htoo Inzali |
| `fine root` | 1 | Htoo Inzali |
| `fine root/` | 1 | Htoo Inzali |
| `help echo` | 1 | Htoo Inzali |
| `echo --help` | 1 | Htoo Inzali |
| `help pwd` | 1 | Htoo Inzali |
| `whatis cat` | 1 | Htoo Inzali |
| `nvm --version` | 1 | Htoo Inzali |
| `mkdir jaime` | 1 | Jaime Romero |
| `cd jaime` | 1 | Jaime Romero |
| `touch codetry.py` | 1 | Jaime Romero |
| `mv hello.rb /root/class03` | 1 | Jaime Romero |
| `cp /root/claa` | 1 | Jaime Romero |
| `cp /root/class03/hello.rb /root/jaime` | 1 | Jaime Romero |
| `cd /root/jaime` | 1 | Jaime Romero |
| `rm /root/jaime/hello.rb` | 1 | Jaime Romero |
| `mv hello.py /root/class03` | 1 | Jaime Romero |
| `cd /root/class03` | 1 | Jaime Romero |
| `rm class03 root` | 1 | Jaime Romero |
| `cat codetry.py` | 1 | Jaime Romero |
| `python codetry.py` | 1 | Jaime Romero |
| `man locate` | 1 | Jaime Romero |
| `locate hello.py` | 1 | Jaime Romero |
| `locate */hello.py` | 1 | Jaime Romero |
| `man compgen` | 1 | Jaime Romero |
| `> file.txt` | 1 | Jaime Romero |
| `cat file.txt` | 1 | Jaime Romero |
| `man >` | 1 | Jaime Romero |
| `man head` | 1 | Jaime Romero |
| `head --lines` | 1 | Jaime Romero |
| `head --lines -5` | 1 | Jaime Romero |
| `man tail` | 1 | Jaime Romero |
| `tail --lines` | 1 | Jaime Romero |
| `man sleep` | 1 | Jaime Romero |
| `sleep 5` | 1 | Jaime Romero |
| `sleep 1m` | 1 | Jaime Romero |
| `kill` | 1 | Jaime Romero |
| `man |` | 1 | Jaime Romero |
| `man echo` | 1 | Jaime Romero |
| `echo "Hello World"` | 1 | Jaime Romero |
| `mkdir prueba` | 1 | Jaime Romero |
| `rmdir prueba` | 1 | Jaime Romero |
| `grep "input" codetry.py` | 1 | Jaime Romero |
| `grep --context|before-context|after-context` | 1 | Jaime Romero |
| `grep --with-filename --line-number` | 1 | Jaime Romero |
| `compgen -c` | 1 | Jaime Romero |
| `time cd jaime` | 1 | Jaime Romero |
| `time python codetry.py` | 1 | Jaime Romero |
| `locate hello` | 1 | Jaime Romero |
| `less codetry.py` | 1 | Jaime Romero |
| `man more` | 1 | Jaime Romero |
| `more codetry.py` | 1 | Jaime Romero |
| `node helloworld.js` | 1 | Ke Ren |
| `cd mila` | 1 | Ke Ren |
| `touch test1.rb` | 1 | Ke Ren |
| `ruby test1.rb` | 1 | Ke Ren |
| `mkdir KeRen` | 1 | Ke Ren |
| `cd KeRen` | 1 | Ke Ren |
| `touch test2.py` | 1 | Ke Ren |
| `python test2.py` | 1 | Ke Ren |
| `touch test3.js` | 1 | Ke Ren |
| `node test3.js` | 1 | Ke Ren |
| `vi .bashhrc` | 1 | Ke Ren |
| `man catch` | 1 | Ke Ren |
| `which ruby` | 1 | Ke Ren |
| `mkdir hello` | 1 | Ke Ren |
| `which xterm` | 1 | Ke Ren |
| `version--python` | 1 | Mia Obralic |
| `python ` | 1 | Mia Obralic |
| `man help` | 1 | Mia Obralic |
| `conda ` | 1 | Mia Obralic |
| `exit() docker stop` | 1 | Mia Obralic |
| `jave --version` | 1 | Miguel Gutierrez |
| `java --version` | 1 | Miguel Gutierrez |
| `man ` | 1 | Miguel Gutierrez |
| `mkdir miguel` | 1 | Miguel Gutierrez |
| `cd miguel` | 1 | Miguel Gutierrez |
| `cal ` | 1 | Miguel Gutierrez |
| `echo "hello, world"` | 1 | Miguel Gutierrez |
| `cal year` | 1 | Miguel Gutierrez |
| `cal -3` | 1 | Miguel Gutierrez |
| `man date` | 1 | Miguel Gutierrez |
| `cd /user` | 1 | Miguel Gutierrez |
| `less miguel` | 1 | Miguel Gutierrez |
| `less root` | 1 | Miguel Gutierrez |
| `less` | 1 | Miguel Gutierrez |
| `less --help` | 1 | Miguel Gutierrez |
| `pwg` | 1 | Miguel Gutierrez |
| `cat hello,py` | 1 | Miguel Gutierrez |
| `cat 'tart -i -a -i -a` | 1 | Miguel Gutierrez |
| `cd C:` | 1 | Noel Joseph |
| `cd C` | 1 | Noel Joseph |
| `cd c:/` | 1 | Noel Joseph |
| `cd c:pwd` | 1 | Noel Joseph |
| `cd c:\1` | 1 | Noel Joseph |
| `cd cache` | 1 | Noel Joseph |
| `cd /.bash_history` | 1 | Noel Joseph |
| `mkdir noel` | 1 | Noel Joseph |
| `cd noel` | 1 | Noel Joseph |
| `ls mann` | 1 | Noel Joseph |
| `mann ls` | 1 | Noel Joseph |
| `ls man` | 1 | Noel Joseph |
| `cp ` | 1 | Noel Joseph |
| `yran --version` | 1 | Omar Yousef |
| `la -F` | 1 | Omar Yousef |
| `ls-lh` | 1 | Omar Yousef |
| `ls -l -t` | 1 | Omar Yousef |
| `mkdir omar` | 1 | Omar Yousef |
| `cd omar` | 1 | Omar Yousef |
| `touch s.rb` | 1 | Omar Yousef |
| `rm s.rb ` | 1 | Omar Yousef |
| `mkdir omar-2` | 1 | Omar Yousef |
| `mkdir omar2` | 1 | Omar Yousef |
| `cd omar2` | 1 | Omar Yousef |
| `cd omar 2` | 1 | Omar Yousef |
| `rub hello.py` | 1 | Omar Yousef |
| `run hell.py` | 1 | Omar Yousef |
| `s` | 1 | Orwa Diraneyya |
| `mkdir orwa` | 1 | Orwa Diraneyya |
| `cd orwa` | 1 | Orwa Diraneyya |
| `code hello.rb ` | 1 | Orwa Diraneyya |
| `nano hello.rb ` | 1 | Orwa Diraneyya |
| `printf` | 1 | Orwa Diraneyya |
| `printf "g"` | 1 | Orwa Diraneyya |
| `console.log` | 1 | Orwa Diraneyya |
| `cat <<EOF` | 1 | Orwa Diraneyya |
| `console.log('Hello, World!');` | 1 | Orwa Diraneyya |
| `EOF` | 1 | Orwa Diraneyya |
| `cat <<EOF > hello.js` | 1 | Orwa Diraneyya |
| `ruby hello.` | 1 | Orwa Diraneyya |
| `cat <<EOF > hello.py` | 1 | Orwa Diraneyya |
| `print("Hello, World!")` | 1 | Orwa Diraneyya |
| `mv orwa class03` | 1 | Orwa Diraneyya |
| `cd class03/` | 1 | Orwa Diraneyya |
| `cd install.sh` | 1 | Ramez Tawfik |
| `cd ~ramez` | 1 | Ramez Tawfik |
| `tocuh root/NEW_FILE` | 1 | Ramez Tawfik |
| `touch newfile` | 1 | Ramez Tawfik |
| `mkdir ramezdirectory` | 1 | Ramez Tawfik |
| `cd ramezdirectory` | 1 | Ramez Tawfik |
| `touch 1_file` | 1 | Ramez Tawfik |
| `touch 2_file 3_file` | 1 | Ramez Tawfik |
| `ls-la` | 1 | Ramez Tawfik |
| `pwd --ramezdirectory` | 1 | Ramez Tawfik |
| `pwd -L -P` | 1 | Ramez Tawfik |
| `mkdir inside_dir` | 1 | Ramez Tawfik |
| `man tocuh` | 1 | Ramez Tawfik |
| `cd inside_dir` | 1 | Ramez Tawfik |
| `cp -v -r` | 1 | Ramez Tawfik |
| `rm ramezdirectory/2_file` | 1 | Ramez Tawfik |
| `cd ramezdirectory/ramezdirectory` | 1 | Ramez Tawfik |
| `rm 2_file` | 1 | Ramez Tawfik |
| `man tpuch` | 1 | Ramez Tawfik |
| `tocuh pythonn.ps` | 1 | Ramez Tawfik |
| `tocuh python` | 1 | Ramez Tawfik |
| `touch python.ps` | 1 | Ramez Tawfik |
| `touch ruby.rb` | 1 | Ramez Tawfik |
| `touch java.js` | 1 | Ramez Tawfik |
| `touch python.py` | 1 | Ramez Tawfik |
| `rm python.ps` | 1 | Ramez Tawfik |
| `ls la` | 1 | Ramez Tawfik |
| `cat python` | 1 | Ramez Tawfik |
| `cat python.py` | 1 | Ramez Tawfik |
| `java.js` | 1 | Ramez Tawfik |
| `cat java.js` | 1 | Ramez Tawfik |
| `python python.py` | 1 | Ramez Tawfik |
| `ruby ruby.rb` | 1 | Ramez Tawfik |
| `node java.js` | 1 | Ramez Tawfik |
| `TOP` | 1 | Ramez Tawfik |
| `exit ` | 1 | Rebeca Garjul |
| `ipy` | 1 | Rebeca Garjul |
| `>>>...` | 1 | Rebeca Garjul |
| `>>>` | 1 | Rebeca Garjul |
| `docker run -i -t` | 1 | Rebeca Garjul |
| ` whoami` | 1 | Rebeca Garjul |
| `ls cd` | 1 | Rebeca Garjul |
| `cd ~Rebeca` | 1 | Rebeca Garjul |
| `touch file{1..5}` | 1 | Rebeca Garjul |
| `cd -d -i -r -e -c -t -o -r -y` | 1 | Rebeca Garjul |
| `cd navigation` | 1 | Rebeca Garjul |
| `cd -n -a -v -i -g -a -t -i -o -n` | 1 | Rebeca Garjul |
| `touch file{1..2}` | 1 | Rebeca Garjul |
| `mv file1` | 1 | Rebeca Garjul |
| `mkdir Test1` | 1 | Rebeca Garjul |
| `mv file1 Test1` | 1 | Rebeca Garjul |
| `cd Test1` | 1 | Rebeca Garjul |
| `mkdir Test2` | 1 | Rebeca Garjul |
| `cp Test1 Test 2` | 1 | Rebeca Garjul |
| `cp Test1 Test2` | 1 | Rebeca Garjul |
| `cp File1 Test2` | 1 | Rebeca Garjul |
| `cp Test1_File1 Test2` | 1 | Rebeca Garjul |
| `cp Test1_file1 Test2` | 1 | Rebeca Garjul |
| `cp -a` | 1 | Rebeca Garjul |
| `cd Test2` | 1 | Rebeca Garjul |
| `mv Test2 root` | 1 | Rebeca Garjul |
| `$ cp file1 newfile` | 1 | Rebeca Garjul |
| `cp file1 newfile` | 1 | Rebeca Garjul |
| `touch file{a..f}` | 1 | Rebeca Garjul |
| `mkdir test2` | 1 | Rebeca Garjul |
| `cd test2` | 1 | Rebeca Garjul |
| `touch file{1..3}` | 1 | Rebeca Garjul |
| `mv test2 root` | 1 | Rebeca Garjul |
| `rm file{1..3}` | 1 | Rebeca Garjul |
| `rm root` | 1 | Rebeca Garjul |
| `rmdir root` | 1 | Rebeca Garjul |
| `rm file2` | 1 | Rebeca Garjul |
| `rmdir Test1` | 1 | Rebeca Garjul |
| `npm` | 1 | Rebeca Garjul |
| `yarn` | 1 | Rebeca Garjul |
| `System.out.println("Hello World!")` | 1 | Rebeca Garjul |
| `cd Rebeca` | 1 | Rebeca Garjul |
| `run hello.rb` | 1 | Rebeca Garjul |
| `cd robin` | 1 | Robin Berweiler |
| `mkdir roibn` | 1 | Robin Berweiler |
| `rm roibn` | 1 | Robin Berweiler |
| `mkdir rm roibn` | 1 | Robin Berweiler |
| `mkdir man` | 1 | Robin Berweiler |
| `mkdir robin` | 1 | Robin Berweiler |
| `chmod u+x hello.js` | 1 | Robin Berweiler |
| `\robin>node hello.js` | 1 | Robin Berweiler |
| `root\robin>node hello.js` | 1 | Robin Berweiler |
| `~\root\robin>node hello.js` | 1 | Robin Berweiler |
| `robin>node hello.js` | 1 | Robin Berweiler |
| `apt` | 1 | Robin Berweiler |
| `apt list` | 1 | Robin Berweiler |
| `man -r` | 1 | Robin Berweiler |
| `man lambo` | 1 | Robin Berweiler |
| `man whomai` | 1 | Robin Berweiler |
| `echo` | 1 | Robin Berweiler |
| `echo -e` | 1 | Robin Berweiler |
| `echo -e -n` | 1 | Robin Berweiler |
| `man mkdir -m` | 1 | Robin Berweiler |
| `mkdir -m` | 1 | Robin Berweiler |
| `mkdir -m --help` | 1 | Robin Berweiler |
| `grep "search_pattern" hello.js` | 1 | Robin Berweiler |
| `mv man` | 1 | Robin Berweiler |
| `mv node` | 1 | Robin Berweiler |
| `mv --help` | 1 | Robin Berweiler |
| `mv node nod` | 1 | Robin Berweiler |
| `mv nod cellar` | 1 | Robin Berweiler |
| `man mc` | 1 | Robin Berweiler |
| `mc` | 1 | Robin Berweiler |
| `mc -b` | 1 | Robin Berweiler |
| `mv hello.py hello.js cellar` | 1 | Robin Berweiler |
| `rm cellar` | 1 | Robin Berweiler |
| `mkdir cellar` | 1 | Robin Berweiler |
| `mv hellos.js hello.py cellar` | 1 | Robin Berweiler |
| `mv hello.js hello.py cellar` | 1 | Robin Berweiler |
| `cd cellar` | 1 | Robin Berweiler |
| `mv hello.py hello.js ..` | 1 | Robin Berweiler |
| `chmod 777 hello.js` | 1 | Robin Berweiler |
| `man history` | 1 | Robin Berweiler |
| `history --help` | 1 | Robin Berweiler |
| `whoami --help` | 1 | Robin Berweiler |
| `mkdir .hidden_cellar` | 1 | Robin Berweiler |
| `cd -h -i -d -d -e -n -_ -c -e -l -l -a -r` | 1 | Robin Berweiler |
| `cd .hidden_cellar` | 1 | Robin Berweiler |
| `man dc` | 1 | Robin Berweiler |
| `dc --expression` | 1 | Robin Berweiler |
| `docker dc --expression` | 1 | Robin Berweiler |
| `dc p` | 1 | Robin Berweiler |
| `docker man` | 1 | Robin Berweiler |
| `man docker` | 1 | Robin Berweiler |
| `docker logs -f` | 1 | Robin Berweiler |
| `apt show` | 1 | Robin Berweiler |
| `apt search` | 1 | Robin Berweiler |
| `man sudo` | 1 | Robin Berweiler |
| `sudo python hello.py` | 1 | Robin Berweiler |
| `sudo less hello.py` | 1 | Robin Berweiler |
| `sudo less ruby hello.rb` | 1 | Robin Berweiler |
| `sudo --shell` | 1 | Robin Berweiler |
| `sudo --lit` | 1 | Robin Berweiler |
| `sudo --list` | 1 | Robin Berweiler |
| `docker sudo --list` | 1 | Robin Berweiler |
| `ruby sudo --list` | 1 | Robin Berweiler |
| `ruby sudo hello.rb` | 1 | Robin Berweiler |
| `pwd -l -p` | 1 | Robin Berweiler |
| `touch -r` | 1 | Robin Berweiler |
| `time` | 1 | Robin Berweiler |
| `time hello.py` | 1 | Robin Berweiler |
| `man -r -a -n -d -o -m` | 1 | Robin Berweiler |
| `man mpc` | 1 | Robin Berweiler |
| `mpc toggle` | 1 | Robin Berweiler |
| `mpc next` | 1 | Robin Berweiler |
| `ls -1 -a` | 1 | Rodrigo Magan |
| `ls -a -d` | 1 | Rodrigo Magan |
| `mkdir rodrigo` | 1 | Rodrigo Magan |
| `cd rodrigo` | 1 | Rodrigo Magan |
| `hello-world.js` | 1 | Rodrigo Magan |
| `rm rodrigo` | 1 | Rodrigo Magan |
| `rmdir rodrigo` | 1 | Rodrigo Magan |
| `Get-Alias ls` | 1 | Rodrigo Magan |
| `ls -3` | 1 | Shady Saleh |
| ` cd .` | 1 | Shady Saleh |
| `mkdir shady` | 1 | Shady Saleh |
| `cd shady` | 1 | Shady Saleh |
| `touch hej.rb` | 1 | Shady Saleh |
| `cd pwd` | 1 | Shady Saleh |
| `cd \root` | 1 | Shady Saleh |
| `quit() root` | 1 | Shady Saleh |
| `quit() docker` | 1 | Shady Saleh |
| `quit` | 1 | Shady Saleh |
| `quit() bash` | 1 | Shady Saleh |
| `python exit` | 1 | Shady Saleh |
| `docker` | 1 | Shady Saleh |
| `whoami#` | 1 | Shady Saleh |
| `cd ~\class03` | 1 | Shady Saleh |
| `mkdir wow` | 1 | Shady Saleh |
| `touch hej` | 1 | Shady Saleh |
| `mv hej /shady` | 1 | Shady Saleh |
| `mv hej /wow` | 1 | Shady Saleh |
| `cd /shady` | 1 | Shady Saleh |
| `rm /hey.rb /wow` | 1 | Shady Saleh |
| `rm hey.rb` | 1 | Shady Saleh |
| `rm /hey.rb` | 1 | Shady Saleh |
| `rm hy` | 1 | Shady Saleh |
| `rm hey` | 1 | Shady Saleh |
| `rm /hey` | 1 | Shady Saleh |
| `version` | 1 | Shady Saleh |
| `mkdir sushmitha` | 1 | Sushmitha Pais |
| `cd sushmitha` | 1 | Sushmitha Pais |
| `ruby -v` | 1 | Sushmitha Pais |
| `type node -v` | 1 | Sushmitha Pais |
| `pthon --version` | 1 | Sushmitha Pais |
| `cd temp` | 1 | Sushmitha Pais |
| `mkdir sushmi` | 1 | Sushmitha Pais |
| `cd sushmi` | 1 | Sushmitha Pais |
| `cd ~sushmi` | 1 | Sushmitha Pais |
| ` cd ~sushmi/` | 1 | Sushmitha Pais |
| `mkdir src` | 1 | Sushmitha Pais |
| `~/sushmi$ pwd` | 1 | Sushmitha Pais |
| `cd /usr` | 1 | Sushmitha Pais |
| `ls sushmi` | 1 | Sushmitha Pais |
| `ls sushmi/include` | 1 | Sushmitha Pais |
| `ls ../util` | 1 | Sushmitha Pais |
| `ls ../lib` | 1 | Sushmitha Pais |
| `mkdir testdel` | 1 | Sushmitha Pais |
| `rm testdel` | 1 | Sushmitha Pais |
| `touch testfile` | 1 | Sushmitha Pais |
| `rm testfile` | 1 | Sushmitha Pais |
| `cd src` | 1 | Sushmitha Pais |
| `cd ../..` | 1 | Sushmitha Pais |
| `cd /root/sushmi` | 1 | Sushmitha Pais |
| `echo $HOME` | 1 | Sushmitha Pais |
| `cd /sushmi` | 1 | Sushmitha Pais |
| `cd/root/sushmi` | 1 | Sushmitha Pais |
| `cat mv` | 1 | Sushmitha Pais |
| `touch testmove` | 1 | Sushmitha Pais |
| `mv testmove` | 1 | Sushmitha Pais |
| `mv src testmove` | 1 | Sushmitha Pais |
| `mv testmove src` | 1 | Sushmitha Pais |
| `ls src` | 1 | Sushmitha Pais |
| `touch example` | 1 | Sushmitha Pais |
| `rm example` | 1 | Sushmitha Pais |
| `cd ~sushmitha` | 1 | Sushmitha Pais |
| `cd ~/sushmitha` | 1 | Sushmitha Pais |
| `cp src/testmove abc` | 1 | Sushmitha Pais |
| `ls abc` | 1 | Sushmitha Pais |
| `diff --report-identical-files` | 1 | Sushmitha Pais |
| `cp abc baz` | 1 | Sushmitha Pais |
| `ls baz` | 1 | Sushmitha Pais |
| `touch testcopy` | 1 | Sushmitha Pais |
| `cp testcopy sushmi` | 1 | Sushmitha Pais |
| `ls sushmi newcopy` | 1 | Sushmitha Pais |
| `cat sushmi` | 1 | Sushmitha Pais |
| `cat testcopy` | 1 | Sushmitha Pais |
| `ls testcopy` | 1 | Sushmitha Pais |
| `ls newcopy` | 1 | Sushmitha Pais |
| `cat newcopy` | 1 | Sushmitha Pais |
| `touch testcat` | 1 | Sushmitha Pais |
| `cat testcopy abc > testcat` | 1 | Sushmitha Pais |
| `ls testcat` | 1 | Sushmitha Pais |
| `cat testcat` | 1 | Sushmitha Pais |
| `cat testcopy abc >> testcat` | 1 | Sushmitha Pais |
| `echo 'I am Sushmitha Pais. I am pursuing my Master's in Germany' >> testcat` | 1 | Sushmitha Pais |
| `command-name >> testcat` | 1 | Sushmitha Pais |
| `echo "I am Sushmitha Pais. I am pursuing my Master's in Germany"` | 1 | Sushmitha Pais |
| ` echo "I am Sushmitha Pais. I am pursuing my Master's in Germany" > file1.txt` | 1 | Sushmitha Pais |
| `ls -a -l` | 1 | Sushmitha Pais |
| `cat file1.txt` | 1 | Sushmitha Pais |
| ` echo "I am excited to be part of this course and I am learning a lot from it" >> file1.txt` | 1 | Sushmitha Pais |
| ` cat -n` | 1 | Sushmitha Pais |
| `docker run -i -t --volumes-from --name` | 1 | Sushmitha Pais |
| `docker run -i -t -v --name` | 1 | Sushmitha Pais |
| `python hellp.py` | 1 | Sushmitha Pais |
| ` python hello.py` | 1 | Sushmitha Pais |
| `ruby hellp.py` | 1 | Sushmitha Pais |
| `ruby hello.py` | 1 | Sushmitha Pais |
| `cat /data/history.txt ` | 1 | Wei Yang |
| `uname` | 1 | Wei Yang |
| `uname -a` | 1 | Wei Yang |
| `touch test` | 1 | Wei Yang |
| `mv test test01` | 1 | Wei Yang |
| `rm test01 ` | 1 | Wei Yang |
| `touch a.txt` | 1 | Wei Yang |
| `echo "hello world!" >> a.txt` | 1 | Wei Yang |
| `cat a.txt` | 1 | Wei Yang |
| `cp a.txt b.txt` | 1 | Wei Yang |
| `cat b.txt ` | 1 | Wei Yang |
| `cat a.txt b.txt >> c.txt` | 1 | Wei Yang |
| `cat c.txt` | 1 | Wei Yang |
| `adduser wei` | 1 | Wei Yang |
| `su wei` | 1 | Wei Yang |
| `ln -s` | 1 | Wei Yang |
| `cd test01` | 1 | Wei Yang |
| `pwd --help` | 1 | Wei Yang |
| `pwd -L` | 1 | Wei Yang |
| `pwd -P` | 1 | Wei Yang |
| `ls --help` | 1 | Wei Yang |
| `cd e/f/g` | 1 | Wei Yang |
| `cd e/f` | 1 | Wei Yang |
| `mv a.txt f.txt` | 1 | Wei Yang |
| `rm b.txt` | 1 | Wei Yang |
| `mkdir foo` | 1 | Wei Yang |
| `mv foo foo01` | 1 | Wei Yang |
| `cp foo01 foo02` | 1 | Wei Yang |
| `mv test test02` | 1 | Wei Yang |
| `ls test` | 1 | Wei Yang |
| `cd hello_ruby/` | 1 | Wei Yang |
| `echo 'puts "Hello, World!"' >> hello.rb ` | 1 | Wei Yang |
| `cd hello_python/` | 1 | Wei Yang |
| `echo 'print("Hello, World!")' > hello.py` | 1 | Wei Yang |
| `cat hello.py ` | 1 | Wei Yang |
| `cd hello_nodejs/` | 1 | Wei Yang |
| `which nano` | 1 | Wei Yang |
| `which vi` | 1 | Wei Yang |
| `cat hello.js ` | 1 | Wei Yang |
| `cls` | 1 | Wei Yang |
| `cd ../hello_python/` | 1 | Wei Yang |
| `cd ../hello_nodejs/` | 1 | Wei Yang |
| `ruby hello_ruby/hello.rb ` | 1 | Wei Yang |
| `python hello_python/hello.py ` | 1 | Wei Yang |
| `node hello_nodejs/hello.js ` | 1 | Wei Yang |
| `Windows PowerShell` | 1 | Ye Lu.txt |
| `版权所有 (C) Microsoft Corporation。保留所有权利。` | 1 | Ye Lu.txt |
| `尝试新的跨平台 PowerShell https://aka.ms/pscore6` | 1 | Ye Lu.txt |
| `PS C:\Users\Thinkpad> docker` | 1 | Ye Lu.txt |
| `Usage: docker [OPTIONS] COMMAND` | 1 | Ye Lu.txt |
| `A self-sufficient runtime for containers` | 1 | Ye Lu.txt |
| `Options:` | 1 | Ye Lu.txt |
| ` --config` | 1 | Ye Lu.txt |
| ` "C:\\Users\\Thinkpad\\.docker")` | 1 | Ye Lu.txt |
| ` -c -, --context` | 1 | Ye Lu.txt |
| ` daemon (overrides DOCKER_HOST env var and` | 1 | Ye Lu.txt |
| ` default context set with "docker context use")` | 1 | Ye Lu.txt |
| ` -D -, --debug` | 1 | Ye Lu.txt |
| ` -H -, --host` | 1 | Ye Lu.txt |
| ` -l -, --log-level` | 1 | Ye Lu.txt |
| ` ("debug"|"info"|"warn"|"error"|"fatal")` | 1 | Ye Lu.txt |
| ` (default "info")` | 1 | Ye Lu.txt |
| ` --tls --tlsverify` | 1 | Ye Lu.txt |
| ` --tlscacert` | 1 | Ye Lu.txt |
| ` "C:\\Users\\Thinkpad\\.docker\\ca.pem")` | 1 | Ye Lu.txt |
| ` --tlscert` | 1 | Ye Lu.txt |
| ` "C:\\Users\\Thinkpad\\.docker\\cert.pem")` | 1 | Ye Lu.txt |
| ` --tlskey` | 1 | Ye Lu.txt |
| ` "C:\\Users\\Thinkpad\\.docker\\key.pem")` | 1 | Ye Lu.txt |
| ` --tlsverify` | 1 | Ye Lu.txt |
| ` -v -, --version` | 1 | Ye Lu.txt |
| `Management Commands:` | 1 | Ye Lu.txt |
| ` builder Manage builds` | 1 | Ye Lu.txt |
| ` buildx* Docker Buildx (Docker Inc., v0.8.2)` | 1 | Ye Lu.txt |
| ` compose* Docker Compose (Docker Inc., v2.4.1)` | 1 | Ye Lu.txt |
| ` config Manage Docker configs` | 1 | Ye Lu.txt |
| ` container Manage containers` | 1 | Ye Lu.txt |
| ` context Manage contexts` | 1 | Ye Lu.txt |
| ` image Manage images` | 1 | Ye Lu.txt |
| ` manifest Manage Docker image manifests and manifest lists` | 1 | Ye Lu.txt |
| ` network Manage networks` | 1 | Ye Lu.txt |
| ` node Manage Swarm nodes` | 1 | Ye Lu.txt |
| ` plugin Manage plugins` | 1 | Ye Lu.txt |
| ` sbom* View the packaged-based Software Bill Of Materials (SBOM) for an image (Anchore Inc., 0.6.0)` | 1 | Ye Lu.txt |
| ` scan* Docker Scan (Docker Inc., v0.17.0)` | 1 | Ye Lu.txt |
| ` secret Manage Docker secrets` | 1 | Ye Lu.txt |
| ` service Manage services` | 1 | Ye Lu.txt |
| ` stack Manage Docker stacks` | 1 | Ye Lu.txt |
| ` swarm Manage Swarm` | 1 | Ye Lu.txt |
| ` system Manage Docker` | 1 | Ye Lu.txt |
| ` trust Manage trust on Docker images` | 1 | Ye Lu.txt |
| ` volume Manage volumes` | 1 | Ye Lu.txt |
| `Commands:` | 1 | Ye Lu.txt |
| ` attach Attach local standard input, output, and error streams to a running container` | 1 | Ye Lu.txt |
| ` build Build an image from a Dockerfile` | 1 | Ye Lu.txt |
| ` commit Create a new image from a container's changes` | 1 | Ye Lu.txt |
| ` cp Copy files/folders between a container and the local filesystem` | 1 | Ye Lu.txt |
| ` create Create a new container` | 1 | Ye Lu.txt |
| ` diff Inspect changes to files or directories on a container's filesystem` | 1 | Ye Lu.txt |
| ` events Get real time events from the server` | 1 | Ye Lu.txt |
| ` exec Run a command in a running container` | 1 | Ye Lu.txt |
| ` export Export a container's filesystem as a tar archive` | 1 | Ye Lu.txt |
| ` history Show the history of an image` | 1 | Ye Lu.txt |
| ` images List images` | 1 | Ye Lu.txt |
| ` import Import the contents from a tarball to create a filesystem image` | 1 | Ye Lu.txt |
| ` info Display system-wide information` | 1 | Ye Lu.txt |
| ` inspect Return low-level information on Docker objects` | 1 | Ye Lu.txt |
| ` kill Kill one or more running containers` | 1 | Ye Lu.txt |
| ` login Log in to a Docker registry` | 1 | Ye Lu.txt |
| ` logout Log out from a Docker registry` | 1 | Ye Lu.txt |
| ` logs Fetch the logs of a container` | 1 | Ye Lu.txt |
| ` pause Pause all processes within one or more containers` | 1 | Ye Lu.txt |
| ` port List port mappings or a specific mapping for the container` | 1 | Ye Lu.txt |
| ` ps List containers` | 1 | Ye Lu.txt |
| ` pull Pull an image or a repository from a registry` | 1 | Ye Lu.txt |
| ` push Push an image or a repository to a registry` | 1 | Ye Lu.txt |
| ` restart Restart one or more containers` | 1 | Ye Lu.txt |
| ` rm Remove one or more containers` | 1 | Ye Lu.txt |
| ` rmi Remove one or more images` | 1 | Ye Lu.txt |
| ` run Run a command in a new container` | 1 | Ye Lu.txt |
| ` save Save one or more images to a tar archive (streamed to STDOUT by default)` | 1 | Ye Lu.txt |
| ` search Search the Docker Hub for images` | 1 | Ye Lu.txt |
| ` stats Display a live stream of container(s) resource usage statistics` | 1 | Ye Lu.txt |
| ` stop Stop one or more running containers` | 1 | Ye Lu.txt |
| ` top Display the running processes of a container` | 1 | Ye Lu.txt |
| ` unpause Unpause all processes within one or more containers` | 1 | Ye Lu.txt |
| ` update Update configuration of one or more containers` | 1 | Ye Lu.txt |
| ` version Show the Docker version information` | 1 | Ye Lu.txt |
| ` wait Block until one or more containers stop, then print their exit codes` | 1 | Ye Lu.txt |
| `Run 'docker COMMAND --help'` | 1 | Ye Lu.txt |
| `To get more help with docker, check out our guides at https://docs.docker.com/go/guides/` | 1 | Ye Lu.txt |
| `PS C:\Users\Thinkpad> run` | 1 | Ye Lu.txt |
| `run : 无法将“run”项识别为 cmdlet、函数、脚本文件或可运行程序的名称。请检查名称的拼写，如果包括路径，请确保路径正确，` | 1 | Ye Lu.txt |
| `然后再试一次。` | 1 | Ye Lu.txt |
| `所在位置 行:1 字符: 1` | 1 | Ye Lu.txt |
| `+ run` | 1 | Ye Lu.txt |
| `+ ~~~` | 1 | Ye Lu.txt |
| ` + CategoryInfo : ObjectNotFound: (run:String) [], CommandNotFoundException` | 1 | Ye Lu.txt |
| ` + FullyQualifiedErrorId : CommandNotFoundException` | 1 | Ye Lu.txt |
| `PS C:\Users\Thinkpad> docker run` | 1 | Ye Lu.txt |
| `"docker run" requires at least 1 argument.` | 1 | Ye Lu.txt |
| `See 'docker run --help'.` | 1 | Ye Lu.txt |
| `Run a command in a new container` | 1 | Ye Lu.txt |
| `PS C:\Users\Thinkpad> docker run -i -t -d -o -c -k -e -r -i -t --rm` | 1 | Ye Lu.txt |
| `unknown shorthand flag: 'o' in -o -c -k -e -r` | 1 | Ye Lu.txt |
| `PS C:\Users\Thinkpad> docker run -i -t --rm` | 1 | Ye Lu.txt |
| `latest: Pulling from orwa84/shell-class` | 1 | Ye Lu.txt |
| `345e3491a907: Pull complete` | 1 | Ye Lu.txt |
| `57671312ef6f: Pull complete` | 1 | Ye Lu.txt |
| `5e9250ddb7d0: Pull complete` | 1 | Ye Lu.txt |
| `81e5969cce2b: Pull complete` | 1 | Ye Lu.txt |
| `ad004616c7ae: Pull complete` | 1 | Ye Lu.txt |
| `6831a5379d01: Pull complete` | 1 | Ye Lu.txt |
| `781f33621212: Pull complete` | 1 | Ye Lu.txt |
| `2ed5f58be5e0: Pull complete` | 1 | Ye Lu.txt |
| `7c04ff23db80: Pull complete` | 1 | Ye Lu.txt |
| `5dcf08ac3ace: Pull complete` | 1 | Ye Lu.txt |
| `e4eee5146e86: Pull complete` | 1 | Ye Lu.txt |
| `d3cfdec8c0c0: Pull complete` | 1 | Ye Lu.txt |
| `57cbd9f3795a: Pull complete` | 1 | Ye Lu.txt |
| `4f4fb700ef54: Pull complete` | 1 | Ye Lu.txt |
| `67c276c271c9: Pull complete` | 1 | Ye Lu.txt |
| `06846566be05: Pull complete` | 1 | Ye Lu.txt |
| `9e249b067434: Pull complete` | 1 | Ye Lu.txt |
| `Digest: sha256:5f8e9a69a838bfef3856388d0caaf9f9192df88331d7a54379b4b838efd2d036` | 1 | Ye Lu.txt |
| `Status: Downloaded newer image for orwa84/shell-class:latest` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ exit` | 1 | Ye Lu.txt |
| `logout` | 1 | Ye Lu.txt |
| `PS C:\Users\Thinkpad> docker run -i -t --name` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ version` | 1 | Ye Lu.txt |
| `bash: version: command not found` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ python-version` | 1 | Ye Lu.txt |
| `bash: python-version: command not found` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ python --version` | 1 | Ye Lu.txt |
| `Python 3.8.5` | 1 | Ye Lu.txt |
| `docker: Error response from daemon: Conflict. The container name "/class-03" is already in use by container "cdbdf259ebcf75b2363336de2c25d115db86d463513d22f00272f0881982363e". You have to remove (or rename) that container to be able to reuse that name.` | 1 | Ye Lu.txt |
| `IMPORTANT: everything you type in this container is recorded to assist in the grading process.` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ docker run -i -t --name` | 1 | Ye Lu.txt |
| `bash: docker: command not found` | 1 | Ye Lu.txt |
| `PS C:\Users\Thinkpad> docker start -a -i` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ ruby,irb erb` | 1 | Ye Lu.txt |
| `bash: ruby,irb: command not found` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ ruby --version` | 1 | Ye Lu.txt |
| `ruby 2.7.0p0 (2019-12-25 revision 647ee6f091) [x86_64-linux-gnu]` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ javascript --version` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ java --version` | 1 | Ye Lu.txt |
| `bash: java: command not found` | 1 | Ye Lu.txt |
| `bash: javascript: command not found` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ node --version` | 1 | Ye Lu.txt |
| `v14.17.0` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ pyton --version` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ docker run -i -t --rm` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ irb` | 1 | Ye Lu.txt |
| `irb(main):001:0> puts('Hello')` | 1 | Ye Lu.txt |
| `Hello` | 1 | Ye Lu.txt |
| ` > nil` | 1 | Ye Lu.txt |
| `irb(main):002:0> exit` | 1 | Ye Lu.txt |
| `~ took 48s` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ cd` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ whoami` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ pwd` | 1 | Ye Lu.txt |
| `/root` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ man` | 1 | Ye Lu.txt |
| ` -a` | 1 | Ye Lu.txt |
| ` -f` | 1 | Ye Lu.txt |
| ` render a local page for testing purposes` | 1 | Ye Lu.txt |
| ` -h -i -s -t -o -r -y` | 1 | Ye Lu.txt |
| ` show the latest search history` | 1 | Ye Lu.txt |
| ` -l -i -s -t -- -a -l -l` | 1 | Ye Lu.txt |
| ` list all available commands for the current platform` | 1 | Ye Lu.txt |
| ` -p` | 1 | Ye Lu.txt |
| ` select platform; supported are: linux, osx, sunos, common` | 1 | Ye Lu.txt |
| ` -p -a -t -h` | 1 | Ye Lu.txt |
| ` -p -l -a -t -f -o -r -m` | 1 | Ye Lu.txt |
| ` -r` | 1 | Ye Lu.txt |
| ` -r -a -n -d -o -m` | 1 | Ye Lu.txt |
| ` prints a random page` | 1 | Ye Lu.txt |
| ` -t` | 1 | Ye Lu.txt |
| ` -u` | 1 | Ye Lu.txt |
| ` -u -p -d -a -t -e` | 1 | Ye Lu.txt |
| ` update local database` | 1 | Ye Lu.txt |
| ` -v` | 1 | Ye Lu.txt |
| ` -v -e -r -s -i -o -n` | 1 | Ye Lu.txt |
| ` print version and exit` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ man cd` | 1 | Ye Lu.txt |
| `Change the current working directory.` | 1 | Ye Lu.txt |
| `More information: <https://manned.org/cd>.` | 1 | Ye Lu.txt |
| ` cd path/to/directory` | 1 | Ye Lu.txt |
| ` cd` | 1 | Ye Lu.txt |
| ` cd ~username` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ man pwd` | 1 | Ye Lu.txt |
| `Print name of current/working directory.` | 1 | Ye Lu.txt |
| `More information: <https://www.gnu.org/software/coreutils/pwd>.` | 1 | Ye Lu.txt |
| ` pwd` | 1 | Ye Lu.txt |
| ` pwd --physical` | 1 | Ye Lu.txt |
| ` pwd --logical` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ cd ~Thinkpad` | 1 | Ye Lu.txt |
| `bash: cd: ~Thinkpad: No such file or directory` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ cd ..` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ man ls` | 1 | Ye Lu.txt |
| `List directory contents.` | 1 | Ye Lu.txt |
| `More information: <https://www.gnu.org/software/coreutils/ls>.` | 1 | Ye Lu.txt |
| ` ls -1` | 1 | Ye Lu.txt |
| ` ls -a` | 1 | Ye Lu.txt |
| ` ls -F` | 1 | Ye Lu.txt |
| ` ls -l -a` | 1 | Ye Lu.txt |
| ` ls -l -S` | 1 | Ye Lu.txt |
| ` ls -l -t -r` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ ls` | 1 | Ye Lu.txt |
| `install.sh` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ cd /` | 1 | Ye Lu.txt |
| `bin data etc lib lib64 media opt root sbin sys tmp var` | 1 | Ye Lu.txt |
| `boot dev home lib32 libx32 mnt proc run srv tldr usr` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ ca tmp` | 1 | Ye Lu.txt |
| `bash: ca: command not found` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ cd tmp` | 1 | Ye Lu.txt |
| `/tmp` | 1 | Ye Lu.txt |
| `tmp.bjfvB9o7O5 tmp.bjfvB9o7O5.tar.gz` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ ls -a` | 1 | Ye Lu.txt |
| `. .. tmp.bjfvB9o7O5 tmp.bjfvB9o7O5.tar.gz` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ ls -F` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ cd .` | 1 | Ye Lu.txt |
| `. .dockerenv boot dev home lib32 libx32 mnt proc run srv tldr usr` | 1 | Ye Lu.txt |
| `.. bin data etc lib lib64 media opt root sbin sys tmp var` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ ls -l -a` | 1 | Ye Lu.txt |
| `total 64` | 1 | Ye Lu.txt |
| `drwxr-xr-x 1 root root 4096 Apr 20 12:57 .` | 1 | Ye Lu.txt |
| `drwxr-xr-x 1 root root 4096 Apr 20 12:57 ..` | 1 | Ye Lu.txt |
| `lrwxrwxrwx 1 root root 7 Apr 16 2021 bin ->` | 1 | Ye Lu.txt |
| `drwxr-xr-x 2 root root 4096 Apr 15 2020 boot` | 1 | Ye Lu.txt |
| `drwxr-xr-x 2 root root 4096 Apr 20 12:57 data` | 1 | Ye Lu.txt |
| `drwxr-xr-x 5 root root 360 Apr 20 13:18 dev` | 1 | Ye Lu.txt |
| `drwxr-xr-x 1 root root 4096 Apr 20 12:57 etc` | 1 | Ye Lu.txt |
| `drwxr-xr-x 2 root root 4096 Apr 15 2020 home` | 1 | Ye Lu.txt |
| `lrwxrwxrwx 1 root root 7 Apr 16 2021 lib ->` | 1 | Ye Lu.txt |
| `lrwxrwxrwx 1 root root 9 Apr 16 2021 lib32 ->` | 1 | Ye Lu.txt |
| `lrwxrwxrwx 1 root root 9 Apr 16 2021 lib64 ->` | 1 | Ye Lu.txt |
| `lrwxrwxrwx 1 root root 10 Apr 16 2021 libx32 ->` | 1 | Ye Lu.txt |
| `drwxr-xr-x 2 root root 4096 Apr 16 2021 media` | 1 | Ye Lu.txt |
| `drwxr-xr-x 2 root root 4096 Apr 16 2021 mnt` | 1 | Ye Lu.txt |
| `drwxr-xr-x 2 root root 4096 Apr 16 2021 opt` | 1 | Ye Lu.txt |
| `dr-xr-xr-x 218 root root 0 Apr 20 13:18 proc` | 1 | Ye Lu.txt |
| `drwx------ 1 root root 4096 Apr 20 13:19 root` | 1 | Ye Lu.txt |
| `drwxr-xr-x 1 root root 4096 Apr 23 2021 run` | 1 | Ye Lu.txt |
| `lrwxrwxrwx 1 root root 8 Apr 16 2021 sbin ->` | 1 | Ye Lu.txt |
| `drwxr-xr-x 2 root root 4096 Apr 16 2021 srv` | 1 | Ye Lu.txt |
| `dr-xr-xr-x 11 root root 0 Apr 20 13:18 sys` | 1 | Ye Lu.txt |
| `drwxr-xr-x 1 root root 4096 Apr 20 10:41 tldr` | 1 | Ye Lu.txt |
| `drwxrwxrwt 1 root root 4096 Apr 20 10:41 tmp` | 1 | Ye Lu.txt |
| `drwxr-xr-x 1 root root 4096 Apr 16 2021 usr` | 1 | Ye Lu.txt |
| `drwxr-xr-x 1 root root 4096 Apr 16 2021 var` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ cd ~` | 1 | Ye Lu.txt |
| `. .bash_history .bash_profile .cache .gitconfig .npm install.sh` | 1 | Ye Lu.txt |
| `.. .bash_logout .bashrc .config .irb_history .profile` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ man mkdir` | 1 | Ye Lu.txt |
| `Creates a directory.` | 1 | Ye Lu.txt |
| `More information: <https://www.gnu.org/software/coreutils/mkdir>.` | 1 | Ye Lu.txt |
| ` mkdir directory` | 1 | Ye Lu.txt |
| ` mkdir directory_1 directory_2 ...` | 1 | Ye Lu.txt |
| ` mkdir -p` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ mkdir luye` | 1 | Ye Lu.txt |
| `install.sh luye` | 1 | Ye Lu.txt |
| `install.sh luye/` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ cd luye` | 1 | Ye Lu.txt |
| `~/luye` | 1 | Ye Lu.txt |
| `. ..` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ man touch` | 1 | Ye Lu.txt |
| `Change a file access and modification times (atime, mtime).` | 1 | Ye Lu.txt |
| `More information: <https://www.gnu.org/software/coreutils/touch>.` | 1 | Ye Lu.txt |
| ` touch path/to/file` | 1 | Ye Lu.txt |
| ` touch -t` | 1 | Ye Lu.txt |
| ` touch -d` | 1 | Ye Lu.txt |
| ` touch -r` | 1 | Ye Lu.txt |
| ` touch path/to/file{1,2,3}.txt` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ touch hello` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ touch hello.rb` | 1 | Ye Lu.txt |
| `~/luye via 💎 v2.7.0` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ code hello.rb` | 1 | Ye Lu.txt |
| `bash: code: command not found` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ apt update` | 1 | Ye Lu.txt |
| `Get:1 http://security.ubuntu.com/ubuntu focal-security InRelease [114 kB]` | 1 | Ye Lu.txt |
| `Hit:2 https://deb.nodesource.com/node_14.x focal InRelease` | 1 | Ye Lu.txt |
| `Hit:3 http://archive.ubuntu.com/ubuntu focal InRelease` | 1 | Ye Lu.txt |
| `Get:4 http://archive.ubuntu.com/ubuntu focal-updates InRelease [114 kB]` | 1 | Ye Lu.txt |
| `Get:5 http://archive.ubuntu.com/ubuntu focal-backports InRelease [108 kB]` | 1 | Ye Lu.txt |
| `Fetched 336 kB in 1s (259 kB/s)` | 1 | Ye Lu.txt |
| `Reading package lists... Done` | 1 | Ye Lu.txt |
| `Building dependency tree` | 1 | Ye Lu.txt |
| `Reading state information... Done` | 1 | Ye Lu.txt |
| `87 packages can be upgraded. Run 'apt list --upgradable'` | 1 | Ye Lu.txt |
| `~/luye via 💎 v2.7.0 took 2s` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ nano hello.rb` | 1 | Ye Lu.txt |
| `bash: nano: command not found` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ sudo apt-get install nano` | 1 | Ye Lu.txt |
| `bash: sudo: command not found` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ ruby hello.rb` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ man cat` | 1 | Ye Lu.txt |
| `Print and concatenate files.` | 1 | Ye Lu.txt |
| `More information: <https://www.gnu.org/software/coreutils/cat>.` | 1 | Ye Lu.txt |
| ` cat file` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ cat hello.rb` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ sudo apt update && sudo apt install nano` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ apt update && apt install nano` | 1 | Ye Lu.txt |
| `Hit:1 https://deb.nodesource.com/node_14.x focal InRelease` | 1 | Ye Lu.txt |
| `Hit:2 http://archive.ubuntu.com/ubuntu focal InRelease` | 1 | Ye Lu.txt |
| `Hit:3 http://archive.ubuntu.com/ubuntu focal-updates InRelease` | 1 | Ye Lu.txt |
| `Hit:4 http://archive.ubuntu.com/ubuntu focal-backports InRelease` | 1 | Ye Lu.txt |
| `Hit:5 http://security.ubuntu.com/ubuntu focal-security InRelease` | 1 | Ye Lu.txt |
| `Suggested packages:` | 1 | Ye Lu.txt |
| ` hunspell` | 1 | Ye Lu.txt |
| `The following NEW packages will be installed:` | 1 | Ye Lu.txt |
| ` nano` | 1 | Ye Lu.txt |
| `0 upgraded, 1 newly installed, 0 to remove and 87 not upgraded.` | 1 | Ye Lu.txt |
| `Need to get 269 kB of archives.` | 1 | Ye Lu.txt |
| `After this operation, 868 kB of additional disk space will be used.` | 1 | Ye Lu.txt |
| `Get:1 http://archive.ubuntu.com/ubuntu focal/main amd64 nano amd64 4.8-1ubuntu1 [269 kB]` | 1 | Ye Lu.txt |
| `Fetched 269 kB in 0s (588 kB/s)` | 1 | Ye Lu.txt |
| `debconf: delaying package configuration, since apt-utils is not installed` | 1 | Ye Lu.txt |
| `Selecting previously unselected package nano.` | 1 | Ye Lu.txt |
| `(Reading database ... 41543 files and directories currently installed.)` | 1 | Ye Lu.txt |
| `Preparing to unpack .../nano_4.8-1ubuntu1_amd64.deb ...` | 1 | Ye Lu.txt |
| `Unpacking nano (4.8-1ubuntu1) ...` | 1 | Ye Lu.txt |
| `Setting up nano (4.8-1ubuntu1) ...` | 1 | Ye Lu.txt |
| `update-alternatives: using /bin/nano to provide /usr/bin/editor (editor) in auto mode` | 1 | Ye Lu.txt |
| `update-alternatives: warning: skip creation of /usr/share/man/man1/editor.1.gz because associated file /usr/share/man/man1/nano.1.gz (of link group editor) doesn't exist` | 1 | Ye Lu.txt |
| `update-alternatives: using /bin/nano to provide /usr/bin/pico (pico) in auto mode` | 1 | Ye Lu.txt |
| `update-alternatives: warning: skip creation of /usr/share/man/man1/pico.1.gz because associated file /usr/share/man/man1/nano.1.gz (of link group pico) doesn't exist` | 1 | Ye Lu.txt |
| `~/luye via 💎 v2.7.0 took 3s` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ nan^C` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯ ^C` | 1 | Ye Lu.txt |
| `⬢ [Systemd] ❯` | 1 | Ye Lu.txt |
| `hello hello.rb` | 1 | Ye Lu.txt |
| `~/luye via 💎 v2.7.0 took 17s` | 1 | Ye Lu.txt |
| `Hello World!!!!!` | 1 | Ye Lu.txt |
| `who am i ` | 1 | Youssef Osama |
| `who am i` | 1 | Youssef Osama |
| `man cs` | 1 | Youssef Osama |
| `docker ps` | 1 | Youssef Osama |
| `vd tmp` | 1 | Youssef Osama |
| `touch hellow` | 1 | Youssef Osama |
| `hello.py` | 1 | Youssef Osama |
| `node hello.ts` | 1 | Youssef Osama |
| `hello.rb ` | 1 | Youssef Osama |
| `mv hello.rb` | 1 | Youssef Osama |
| `mv hello.rb class03` | 1 | Youssef Osama |
| `cd class03 ` | 1 | Youssef Osama |
| `run ps` | 1 | Youssef Osama |
| `cp class03 hello.py` | 1 | Youssef Osama |
| `cp /root/class03/hello.rb root/class03/directory` | 1 | Youssef Osama |
| `cp /root/class03/hello.rb /root/class03/directory` | 1 | Youssef Osama |
| `cd directory` | 1 | Youssef Osama |
| `mkdir YuWang` | 1 | Yu Wang |
| `cd YuWang` | 1 | Yu Wang |
| `car hello.rb` | 1 | Yu Wang |
| `touch YuWang.py` | 1 | Yu Wang |
| `python YuWang.py` | 1 | Yu Wang |
| `touch YuWang.js` | 1 | Yu Wang |
| `node YuWang.js` | 1 | Yu Wang |