import re
import os 
import markdown
from datetime import datetime

# the final dictionary result object (to be exported as JSON)
results = {}
students = []

md_output = "# Shell-Class Assignment Report (using DevOps)\n\n"
md_output += "Generated on " + datetime.now().strftime("%d/%m/%Y %H:%M:%S") + "\n\n"
md_output += "## Violators of the Contribution Guidelines 👿\n\n" 
violations = 0

for filename in os.listdir("."):
    if not filename.endswith('.txt'):
        continue

    matches = re.match(r'history-([a-z]+)_([a-z]+)\.txt', filename)
    if matches:
        student = matches.group(1).capitalize() + " " + matches.group(2).capitalize()
        students.append(student)
    else:
        md_output += f"- `{filename}`\n"
        violations += 1
        continue

    f = open(filename)
    file_lines = f.read().split('\n')

    # remove new session lines
    while "--- NEW SESSION ---" in file_lines:
        file_lines.remove("--- NEW SESSION ---")


    count = 0
    for command in file_lines:
        if not command or command == "":
            continue
        command_parts = re.split(r'(?:\s+|=)', command)

        command_head = ""
        command_options = []

        i = 0
        # skip through commands in the beginning
        while i < len(command_parts):
            if command_parts[i] and command_parts[i][0] == '-':
                break
            else:
                command_head += command_parts[i] + " "
            i = i + 1

        if (i > 0):
            command_head = command_head[:-1]
        else:
            # No command found on that line
            continue

        # Only commands found on that line, means
        # it was a combination of a command and targets
        if i >= len(command_parts):
            command_head = command_parts[0]
        else:
            while i < len(command_parts):
                if command_parts[i].startswith("--"):
                    command_options.append(command_parts[i][2:])
                elif command_parts[i].startswith("-"):
                    for option in command_parts[i][1:]:
                        command_options.append(option)
                i = i + 1

        # print("original line: ", command)
        # print("command part: ", command_head)
        # print("command options: ", command_options)

        # count = count + 1
        # if count > 5:
        #     break

        unique_handle = command_head
        if len(command_options) > 0:
            for i in range(len(command_options)):
                if len(command_options[i]) == 1:
                    command_options[i] = '-' + command_options[i]
                elif len(command_options[i]) > 1:
                    command_options[i] = '--' + command_options[i]

            unique_handle += ' ' + ' '.join(command_options)
        
        if unique_handle not in results:
            results[unique_handle] = set()

        results[unique_handle].add(student)



# print(f"| `command` | number of doers | names of doers |")
# print( "| --------- | --------------- | -------------- |")
# for command, doers in sorted(results.items(), key=lambda x: -len(x[1])):
#     print(f"| `{command}` | {len(doers)} | {', '.join(doers)} |")

if violations == 0:
    md_output += "\n\nNo violations found in the repository 😀\n\n"
else:
    md_output += "\n\nUnfortunately, DevOps people are very not nice when violations are made to their guidelines, hence these files were not graded 🙈. The algorithm is cruel 😭\n\n"

md_output += "## List of the Dos and the Doers\n\n"
md_output += f"| `command` | doers | names of doers |\n"
md_output += "| --------- | --------------- | -------------- |\n"
for command, doers in sorted(results.items(), key=lambda x: -len(x[1])):
    md_output += f"| `{command}` | {len(doers)} | {', '.join(doers)} |\n"

html = markdown.markdown(md_output, extensions=['extra'])

print("""
<!DOCTYPE md_output>
<md_output lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DSSIPD Shell-Class Report</title>
    <link rel="stylesheet" href="markdown.css">
    <style>
        thead tr th:first-child,
        tbody tr td:first-child {{
            max-width: 200px;
        }}
    </style>
  </head>
  <body>
	{html}
  </body>
</md_output>
""".format(html=html))
