# Grading Report

Can be found [here](https://rwth-crmasters-sose22.gitlab.io/course-work/shell-history/)

# Contribution Guidlines

If you do not know what _Contribution guidelines_ are, you might want to check the respository named `gitlab-markdown` in the same GitLab group.

Similar to that repository, this repository has a single guideline, which is the filenaming policy below.

## Filenaming policy

You must name files in the following way:  

    history-first_last.txt

where `history` is a literal text and both `first` and `last` are placeholders defined as follows:
- `first`: is your first (or given) name. This is the name that you do not share with other members of the family.
- `last`: is your last (or family) name. This is the name that you do share with other members of the family.

### Details

- All place holders should be typed in small letters.
- If the `first` or `last` placeholders contain compound names (i.e. names that has two or more words), then choose the one word that represents best.
- If the `first` or `last` placeholders contain non-latin characters (like the _Umlaute_ letters in German ä, ö, or ü), replace these non-latin characters with their latin equivalents (i.e. "ae", "oe", or "ue" for ä, ö or ü). If not possible, substitute them with a latin approximation (e.g. Ç → c)
